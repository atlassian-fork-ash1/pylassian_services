"""Wrappers for accessing GIT server.
"""
import logging
import slumber

logger = logging.getLogger(__name__)

ClientError = slumber.exceptions.HttpClientError
ServerError = slumber.exceptions.HttpServerError


class StashService(object):
    """Represents a Stash server used by a specific user.
    """
    def __init__(self, url, auth):
        self.url = url
        self.api = slumber.API(self.url + "/rest/api/latest", auth=auth, append_slash=False)

    def get_commits_in_range(self, until_commit, repository_path,from_commit=None):
        if from_commit:
            return self.api.projects(repository_path).commits.get(since=from_commit, until=until_commit)
        else:
            return self.api.projects(repository_path).commits.get(limit=10, until=until_commit)


class BitBucketService(object):
    """Represents a Bitbucket server used by a specific user.
    """
    def __init__(self, url, auth):
        self.url = url
        self.api = slumber.API(self.url + "/api/2.0/", auth=auth, append_slash=False)

    def get_commits_in_range(self, until_commit, repository_path, from_commit=None):
        if from_commit:
            return self.api.repositories(repository_path).commits(until_commit).get(exclude=from_commit, pagelen=100)
        else:
            return self.api.repositories(repository_path).commits(until_commit).get(pagelen=10)