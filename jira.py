import slumber


class JiraService(object):
    """Wrapper that adds functionality on top of the basic JIRA REST API.
    """

    def __init__(self, url, auth):
        self.url = url
        self.service = slumber.API(
            self.url + "/rest/api/2",
            auth=auth,   # TODO change this to minion bot creds
            append_slash=False
        )

    def get_ticket_details(self, key):
        return self.service.issue(key).get()

    def get_custom_fields(self):
            return self.service.field.get()


    def fetch_field_id_for_name(self, name):
        """
        Gets the field ID of a custom field with the given name. Raises exception if duplicate custom fields with the same
        name are found
        :param jira_service: Object of the jira service class
        :param name: Name of the custom field
        :return: The field ID of the cstom field as "customfield_fieldID"
        """
        all_fields = self.get_custom_fields()
        field_ids = []
        for item in all_fields:
            if item['clauseNames'] and item['clauseNames'][0] == name:
                field_ids.append(item['schema']['customId'])

        return field_ids
