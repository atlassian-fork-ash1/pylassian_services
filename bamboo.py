"""Access to Bamboo via REST API.

Reference: https://docs.atlassian.com/bamboo/REST/5.8.0

(c) 2015 Atlassian Pty Ltd ALL RIGHTS RESERVED
"""
import datetime
from os import path
import urlparse
import requests
import slumber
from slumber.exceptions import HttpClientError, HttpServerError
from OpenSSL.SSL import SysCallError
from pylassian import ResilientSession, CachedResource, logger


def bamboo_variables(variables):
    """Change names of variables to equivalent Bamboo identifiers.
    """
    if variables is None:
        return dict()
    return dict([("bamboo.variable." + k, v) for (k, v) in variables.items()])


def plan_build_result(url):
    """Make sure given url is for a plan build result by removing any job key.
    """
    # Separate result key into bits.
    root, key = url.rsplit('/', 1)
    bits = key.split('-')
    # If key is for a job result then remove job key to get plan build result
    # otherwise leave it unchanged as it must already be a plan result.
    if len(bits) > 3:
        key = '-'.join([bits[0], bits[1], bits[3]])
        return root + '/' + key
    else:
        return url


class BambooService(object):
    """Objects for interacting with a Bamboo server via its REST API.
    """
    def __init__(self, server, auth):
        self.auth = auth
        self.server = server
        self.host = urlparse.urlparse(server).hostname
        self.api = slumber.API(server + "/rest/api/latest", session=ResilientSession(), auth=auth)

    def item_from_uri(self, uri, query=None):
        """Return object identified by given URI and optional query string.
        """
        # Check url path to determine type of object to return.
        if not uri:
            return self
        elif uri.startswith("/browse/"):
            # Get key from path and decide whether it's a build plan or result.
            key = path.basename(uri)
            keys = key.split('-')
            # Bamboo plan url injected has "release" between plan key and build number
            if len(keys) >= 3:
                plan = BambooBuildPlan(bamboo=self, key="%s-%s" % (keys[0], keys[1]))
                return BambooBuildResult(plan, number=int(keys[-1]), key=key)
            elif len(keys) == 2:
                return BambooBuildPlan(bamboo=self, key=key)
            else:
                raise ValueError("Unrecognised key in URL")
        elif uri == "/deploy/viewDeploymentResult.action":
            # Get result ID from query string and return deployment object.
            args = urlparse.parse_qs(query or "")
            return BambooDeploymentResult(bamboo=self, number=args['deploymentResultId'][0])
        else:
            raise ValueError("Unrecognised Bamboo page URL")

    def plan(self, key):
        return BambooBuildPlan(self, key)

    def deployment(self, deploy_id):
        return BambooDeploymentResult(self, deploy_id)


class BambooBuildPlan(CachedResource):
    """Objects representing Bamboo build plans.
    """
    def __init__(self, bamboo, key, state=None):
        super(BambooBuildPlan, self).__init__(state)
        self.bamboo = bamboo
        self.key = key

    def current_state(self, **kwargs):
        return self.bamboo.api.plan(self.key).get(**kwargs)

    def is_building(self):
        # Always get current state when checking if building.
        return self.refresh().isBuilding

    def results(self, **kwargs):
        reply = self.bamboo.api.result(self.key).get(**kwargs)
        found = []
        for item in reply['results']['result']:
            result = BambooBuildResult(
                plan=self,
                number=item['buildNumber'],
                key=item['key'],
                state=item,
            )
            found.append(result)
        return found

    def run(self, variables=None, labels=None):
        """Run plan with given variables and return BuildResult with given labels.
        """
        # Construct variable name parameters by adding prefix.
        kwargs = bamboo_variables(variables)
        # Start build and create result to track it.
        reply = self.bamboo.api.queue(self.key).post({}, **kwargs)
        result = BambooBuildResult(self, reply['buildNumber'], reply['buildResultKey'])
        # Add given labels to build result.
        if labels:
            result.add_labels(*labels)
        return result

    def deployment_project(self):
        reply = self.bamboo.api.deploy.project.forPlan.get(planKey=self.key)
        return BambooDeploymentProject(self.bamboo, reply[0]['id']) if reply else None


class BambooBuildResult(CachedResource):
    """Objects representing Bamboo build results.
    """
    def __init__(self, plan, number, key=None, state=None):
        super(BambooBuildResult, self).__init__(state)
        self.plan = plan
        self.number = number
        self.key = key or "%s-%d" % (self.plan.key, self.number)

    def current_state(self, **kwargs):
        """Refresh state of build from Bamboo if not finished.
        """
        # If build finished then always return cached state.
        if self._state and self.finished:
            return self._state
        return self.plan.bamboo.api.result(self.key).get(**kwargs)

    def is_queued(self):
        """True if build is queued and has not started yet.

        :returns: True if build is queued
        """
        # Fast status will include queued time if queued.
        if self.is_running():
            return self.prettyQueuedTime is not None
        else:
            return False

    def is_running(self):
        """Quick refresh of partial status for running build (less load on Bamboo).

        :returns: True if build is queued or started
        """
        # Finished if cached state says so.
        if self._state and self._state.get('finished'):
            return False
        # Otherwise do a fast status check:
        # 200 => running (jobs might be queued)
        # 404 => not running
        try:
            self._state = self.plan.bamboo.api.result.status(self.key).get()
            return True
        except HttpClientError as e:
            if e.response.status_code == 404:
                # Finished but we only have minimal state so get full final state.
                self.stale().refresh()
                return False
            raise
        except SysCallError:
            # If we get a transient SSL error treat result as still running so we retry.
            return True

    def is_successful(self):
        return not self.is_running() and self.successful

    def rerun(self, variables=None):
        """Re-run failed jobs for this build optionally with given variables.
        """
        # Construct variable name parameters by adding prefix.
        kwargs = bamboo_variables(variables)
        # Start build and create result to track it.
        self.plan.bamboo.api.queue(self.key).put({}, **kwargs)
        self.stale()
        return self

    @property
    def web_url(self):
        return "%s/browse/%s" % (self.plan.bamboo.server, self.key)

    @property
    def duration(self):
        """Get duration so far in seconds.
        """
        # Get time from progress if build is still running.
        if 'progress' in self.state:
            secs = int(self.progress['buildTime'] / 1000)
        else:
            secs = self.buildDurationInSeconds
        return datetime.timedelta(seconds=secs)

    def timer(self):
        """Get description of current progress in terms of time (or empty string if not InProgress).

        Example: "20 mins done and 140 mins remaining (12%)"
        """
        if 'progress' not in self.state:
            return "no progress info"
        return (
            "{prettyBuildTime} done and {prettyTimeRemaining}"
            " ({percentageCompletedPretty})"
        ).format(**self.progress)

    def artefacts(self):
        """Get dict of artefact details indexed by name.
        """
        # Make sure we have current state with artefact details.
        if self._state is None or "artifact" not in self.artifacts:
            self.refresh(expand="artifacts")
        if "artifact" in self.artifacts:
            return {a['name']: a for a in self.artifacts['artifact']}
        else:
            return {}

    def add_labels(self, *args):
        """Add one or more labels to this result.

        Strings will be converted to lowercase with spaces removed and other characters transformed.
        """
        for name in args:
            try:
                # Replace dots with dashes otherwise Bamboo replaces with underscores.
                self.plan.bamboo.api.result(self.key).label.post({"name": name.replace('.', '-')})
            except ValueError:
                pass
            except HttpServerError as e:
                if e.response.status_code == 500:
                    logger.info("Could not label the release plan, Passing the step as it is not such a bad failure")
                    pass

        return self

    def result_log(self, job):
        url = "%s/download/%s-%s/build_logs/%s-%s-%d.log" % (
            self.plan.bamboo.server, self.plan.key, job, self.plan.key,
            job, self.number,
        )
        return requests.get(url, stream=True, auth=self.plan.bamboo.auth)

    def add_comment(self, content):
        self.plan.bamboo.api.result(self.key).comment.post({'content': content})
        return self


class BambooDeploymentProject(CachedResource):
    """Objects representing a Bamboo deployment project.
    """
    def __init__(self, bamboo, number, plan=None, state=None):
        super(BambooDeploymentProject, self).__init__(state)
        self.bamboo = bamboo
        self.number = number
        self.plan = None
        self._environments = None

    def current_state(self, **kwargs):
        return self.bamboo.api.deploy.project(self.number).get()

    def environment(self, name):
        if self._environments is None:
            self._environments = {
                e['name']: BambooDeploymentEnvironment(self, state=e) for e in self.state['environments']
            }
        return self._environments[name]


class BambooDeploymentRelease(CachedResource):
    """Objects representing a release in a Bamboo deployment project.

    Note that Bamboo doesn't curently provide a way to get the state of
    a release so these must be created using state contained within other
    resource, e.g. in deployment results for an environment.
    """
    def __init__(self, project, number, state):
        super(BambooDeploymentRelease, self).__init__(state)
        self.project = project
        self.number = number
        self._build = None
        self._status = None

    @property
    def status(self, status=None):
        """Get or set approval status of release.
        """
        if status:
            self.project.bamboo.api.deploy.version(self.number).status(status).post()
            return self
        else:
            reply = self.project.bamboo.api.deploy.version(self.number).status.get()
            return reply['versionState'] if reply else None

    @property
    def build(self):
        if self._build is None:
            assert len(self.items), "no artefacts to get plan key from"
            ids = self.items[0]['planResultKey']
            self._build = BambooBuildResult(
                self.project.plan,
                ids['resultNumber'],
                key=ids['key'],
            )
        return self._build

    def issues_link(self):
        """Get URL of "Issues" view for this release.
        """
        return (
            "{service}/deploy/viewDeploymentVersionJiraIssues.action"
            "?versionId={release}&deploymentProjectId={project}"
        ).format(
            service=self.project.bamboo.server,
            release=self.number,
            project=self.project.number,
        )


class BambooDeploymentEnvironment(CachedResource):
    """Objects representing an environment in a Bamboo deployment project.

    Note: has no "name" property because Bamboo rarely provides this for environments.
    """
    def __init__(self, project, number=None, state=None):
        """Create from number or state.

        WARNING: If created from number only then other properties will be missing
        because Bamboo has no REST resource for environment state,
        only one to get a list of deployments.
        """
        super(BambooDeploymentEnvironment, self).__init__(state)
        self.project = project
        self.number = self._state['id'] if state else number
        self.deploying = None

    def current_state(self, **kwargs):
        # No way to get current state if not passed into constructor.
        raise NotImplementedError("Cannot get current state of environment via REST")

    def is_deploying(self):
        # Get deployments to given environment and see if latest is running.
        self.deployments()
        return self.deploying is not None

    def deployments(self):
        # Always get latest list of deployments.
        reply = self.project.bamboo.api.deploy.environment(self.number).results.get()
        deploys = [
            BambooDeploymentResult(self.project.bamboo, deploy['id'], deploy)
            for deploy in reply['results']
        ]
        # Remember the latest deployment if still running.
        if deploys and deploys[0].is_deploying():
            self.deploying = deploys[0]
        else:
            self.deploying = None
        return deploys

    def previous_release(self):
        deployed = self.deployments()
        logger.debug(
            "Previously released to %s: %s",
            self.number,
            ','.join([deploy.release.name for deploy in deployed]),
        )
        if len(deployed) > 1:
            return deployed[1].release
        else:
            return None


class BambooDeploymentResult(CachedResource):
    """Objects representing a Bamboo deployment result.
    """
    def __init__(self, bamboo, number, state=None):
        super(BambooDeploymentResult, self).__init__(state)
        self.bamboo = bamboo
        self.number = number
        self._release = None
        self._environment = None
        self._deployment_key = None

    def current_state(self, **kwargs):
        # If deployment finished then always return cached state.
        if self._state and self._state['lifeCycleState'] == "FINISHED":
            return self._state
        return self.bamboo.api.deploy.result(self.number).get(**kwargs)

    def is_deploying(self):
        return self.refresh().lifeCycleState != "FINISHED"

    def is_successful(self):
        return self.lifeCycleState == "FINISHED" and self.deploymentState == "SUCCESS"

    def _get_related_items(self):
        # Get status and pull out some key fields.
        project_id, environment_id = map(int, self.key['entityKey']['key'].split('-'))
        release = self.deploymentVersion
        # Create items related to this deployment result.
        project = BambooDeploymentProject(self.bamboo, project_id)
        self._release = BambooDeploymentRelease(project, release['id'], release)
        self._environment = BambooDeploymentEnvironment(project, number=environment_id)
        self._deployment_key = self.key["key"]

    @property
    def deployment_key(self):
        if self._deployment_key is None:
            self._get_related_items()
        return self._deployment_key

    @property
    def release(self):
        if self._release is None:
            self._get_related_items()
        return self._release

    @property
    def environment(self):
        if self._environment is None:
            self._get_related_items()
        return self._environment

    def issues_link(self):
        """Get URL of "Issues" view for release compared with any previous release to same environment.
        """
        url = self.release.issues_link()
        last = self.environment.previous_release()
        if last:
            return url + "&compareWithVersion={compare}".format(compare=last.name)
        else:
            return url

    def result_log(self, job=None):
        url = "%s/deployment-download/%s/build_logs/%s.log" % (
            self.bamboo.server,
            self.environment.number,
            self._deployment_key,
        )
        return requests.get(url, auth=self.bamboo.auth, stream=True)


def item_from_url(url, auth):
    """Create object represented by given URL.
    - BambooService
    - BambooDeploymentResult
    - BambooBuildResult
    - BambooBuildPlan
    """
    # Explode url into bits and make a Bamboo server that can return items.
    bits = urlparse.urlparse(url)
    host = BambooService("%s://%s" % (bits.scheme, bits.netloc), auth=auth)
    return host.item_from_uri(uri=bits.path, query=bits.query)
