"""Stuff for interacting with Bitbucket services.
"""
from urlparse import urlparse


def git_url_ssh_to_http(url):
    """Converts given clone URL of a git repository into http URL.

    Bitbucket Cloud

        git@bitbucket.org:atlassian/freezer-plan-templates.git
        https://bitbucket.org/atlassian/freezer-plan-templates

    Bitbucket Server (Stash)

        ssh://git@stash.atlassian.com:7997/pipe/minion.git
        https://stash.atlassian.com/projects/pipe/repos/minion

    :param url: git URL of the repository
    :return: http URL of the git repository
    :raise: ValueError if not a valid git url
    """
    # Return blank if no url given.
    if not url:
        return ""
    # Return url if already http.
    if url.startswith("http"):
        return url
    # Convert git uri to ssh url.
    if not url.startswith("ssh:"):
        bits = url.split(':', 1)
        url = "ssh://%s/%s" % (bits[0], bits[1])
    # Convert ssh url to http url.
    bits = urlparse(url)
    user = bits.username
    if user != "git":
        raise ValueError("Wrong username: %s" % user)
    path = bits.path
    if not path.endswith(".git"):
        raise ValueError("Not a git path: %s" % path)
    path = path.rsplit('.', 1)[0]
    host = bits.hostname
    if host == "bitbucket.org":
        return "https://%s%s" % (host, path)
    elif host == "stash.atlassian.com":
        path = path.split('/')
        return "https://%s/projects/%s/repos/%s" % (host, path[1], path[2])
    else:
        raise ValueError("Unknown repository: %s" % host)
