"""Package of modules that help with using other services, e.g. JIRA, Bamboo, AWS.

These define slumber API objects either on their own or wrapped in classes
that add functionality not provided by the raw REST API.
"""
from logging import getLogger, FileHandler, config
from time import sleep
from requests import Session
from sys import stdout
from requests.exceptions import ConnectionError
import pytz
import datetime
from dateutil.parser import parse

# Capture logging from this module in a file
# so we can publish as an artifact for analysis.
# Notes:
# - Logger name must be below this package otherwise all modules in package will use it.
# - Open log file in w mode so we get a new log each time.
# - Does not propagate to freezer.logger so we don't see this stuff on console.
rslog = "resilient-rest.log"
logger = getLogger(__name__ + ".session")
logger.addHandler(FileHandler(rslog, mode='w'))
logger.propagate = False

utc_format = "%Y-%m-%dT%H:%M:%SZ"

# Configure logging to write short messages for INFO and above to stdout.

logging = {
    'version': 1,
    'formatters': {
        'default': {
            'format': "%(levelname)8s: %(message)s",
            # 'datefmt': "%Y-%m-%d %H:%M:%S.%f",  <= %f does not show microseconds
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'stream': stdout,
        }
    },
    'root': {
        'handlers': ['console'],
        'level': "INFO",
    },
    'loggers': {
        'requests.packages.urllib3.connectionpool': {
            'level': "WARNING",  # hide low-level comms logging
        }
    }
}

config.dictConfig(logging)


class ResilientSession(Session):
    """Special type of HTTP Session that will retry requests that return transient errors.

    Can be used with REST API wrappers like this:

        api = slumber.API(url, session=ResilientSession(), auth=(username, password))

    Will retry any request that returns timeout or HTTP 501/502/503 errors,
    waiting longer each time until retry limit is reached.
    """
    # Number of times to retry HTTP requests and
    # seconds to add to wait time on each retry.
    retries = 20
    backoff = 5
    maximum = retries * backoff

    def send(self, request, **kwargs):
        """Add retries with back-off to standard send functionality.
        """
        wait = self.backoff
        while wait:
            try:
                logger.debug(">> Sending request %s %s", request.method, request.url)
                response = super(ResilientSession, self).send(request, **kwargs)
                if response.status_code in (502, 503, 504) and wait < self.maximum:
                    logger.warning("!! Retrying %s %s on %d error", request.method, request.url, response.status_code)
                else:
                    logger.debug("<< Got response %d in %s", response.status_code, response.elapsed)
                    return response
            except ConnectionError as e:
                if wait < self.maximum:
                    logger.warning("!! Retrying %s %s on ConnectionError: %s", request.method, request.url, e)
                else:
                    raise
            sleep(wait)
            wait += self.backoff


class CachedResource(object):
    """Abstraction for objects that hold cached state.
    """
    def __init__(self, state=None):
        self._state = state

    def __getattr__(self, item):
        return self.state.get(item)

    @property
    def state(self):
        """Current state after refresh if stale.
        """
        if self._state is None:
            self.refresh()
        return self._state

    def refresh(self, **kwargs):
        """Refresh cached state from source.
        """
        self._state = self.current_state(**kwargs)
        return self

    def stale(self):
        """Clear cached state because it is now out of date.
        """
        self._state = None
        return self

    def current_state(self, **kwargs):
        """Implement this to return current state.
        """
        raise NotImplementedError


def utc_from_clock():
    """Get time in UTC.

    :return: current UTC time to nearest second
    :rtype: datetime.datetime
    """
    return pytz.utc.localize(datetime.datetime.utcnow().replace(microsecond=0))


def utc_from_str(text):
    """Get UTC time from given ISO string.

    :param text: string containing ISO UTC timestamp
    :return: text converted to UTC time to nearest second
    :rtype: datetime.datetime
    """
    return parse(text).replace(microsecond=0).astimezone(pytz.utc)

def explode_artifacts(artifacts):
    """Convert product artifacts string into a python structure.

    :param artifacts: e.g. "application,plugin:application,group/plugin:application+application"
    :return: e.g. [application,
                   [plugin, [application]],
                   [plugin, [application, application], group]]
    """
    delta = list()
    for item in artifacts.split(','):
        bits = item.split(':')
        if len(bits) == 1:
            # just app
            delta.append(item)
        else:
            apps = bits[1].split('+')
            bits = bits[0].split('/')
            if len(bits) == 1:
                # plugin and apps
                delta.append((bits[0], apps))
            else:
                # plugin, apps and group
                delta.append((bits[1], apps, bits[0]))
    return delta